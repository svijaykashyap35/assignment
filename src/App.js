import React, { Component } from 'react';

import './App.css';

class App extends Component {

state = {
    roomCount : 1,
    adultCount : 1,
    childCount : 0,
    
};

roomIncrement = () => {
    let { roomCount, adultCount, childCount} = this.state;
    if(roomCount === adultCount && roomCount < 5 && childCount === 0) {
        this.setState({adultCount: this.state.adultCount + 1, roomCount: this.state.roomCount + 1})
    } else {
        this.setState({roomCount: this.state.roomCount + 1})
    }
}

adultIncrement = () => {
    let { roomCount, adultCount, childCount} = this.state;
    if(adultCount + childCount === roomCount * 4 && roomCount < 5) {
        this.setState({adultCount: this.state.adultCount + 1, roomCount: this.state.roomCount + 1})
    } else {
        this.setState({adultCount: this.state.adultCount + 1})

    }

}

childIncrement = () => {
    let { roomCount, adultCount, childCount} = this.state;
    if(adultCount + childCount === roomCount * 4 && roomCount < 5) {
        this.setState({childCount: this.state.childCount + 1, roomCount: this.state.roomCount + 1})
    } else {
        this.setState({childCount: this.state.childCount + 1})

    }
}



roomDecrement = () => {
    let { roomCount, adultCount, childCount} = this.state;
    if(adultCount - 4 <= 0 && roomCount > 1 && childCount == 0) {
        this.setState({roomCount: this.state.roomCount - 1})
    }else if(childCount <= 4) {
        this.setState({roomCount: this.state.roomCount - 1, childCount: 0, adultCount: this.state.adultCount - (4 - childCount)})
    } else if(childCount > 4){
        console.log('hii');
        this.setState({roomCount: this.state.roomCount - 1, childCount: this.state.childCount - 4})
    }
}

adultDecrement = () => {
    let { roomCount, adultCount} = this.state;
    if(adultCount > roomCount) {
        this.setState({adultCount: this.state.adultCount - 1});
    } else if( adultCount === roomCount) {
        this.setState({adultCount: this.state.adultCount - 1, roomCount: this.state.roomCount - 1});
    }
}


childDecrement = () => {
    this.setState({childCount: this.state.childCount - 1});
}
// roomDecrement = () => {
//     if(this.state.roomCount <= 1)
//     {
//         alert("minimum roomcount reached")
//     }
//     else
//     {
//         this.setState({roomCount : (this.state.roomCount-1), totalCount : ((this.state.roomCount-1)*4)})
    
//         if(this.state.adultCount>((this.state.roomCount-1)*4))
//         {
//             this.setState({adultCount : ((this.state.roomCount-1)*4),childCount : 0})
//         }
//     }
// }

// adultIncrement = () => {

// }

// adultIncrement = () => {
//     console.log(this.state.roomCount)
//     console.log(this.state.totalCount)

//     this.setState({availableCount : (this.state.totalCount - this.state.childCount)})
//     if((this.state.adultCount == this.state.totalCount)&&(this.state.adultCount<this.state.availableCount))
//     {
//         alert("adult count limit reached")
//     }
//     else if(this.state.adultCount < this.state.totalCount)
//     {
//         this.setState({adultCount : (this.state.adultCount+1),presentCount : (this.state.presentCount + (this.state.adultCount+1))})
//     }
// }

// adultDecrement = () => {
//     if(this.state.adultCount == this.state.roomCount)
//     {
//         alert("cannot be decremented")
//     }
//     else
//     {
//         this.setState({adultCount : (this.state.adultCount-1),presentCount : this.state.presentCount-(this.state.adultCount-1),availableCount : (this.state.availableCount+1)})
//     }
// }

// childIncrement = () => {

//     this.setState({availableCount : (this.state.totalCount - this.state.adultCount)})

//     if(this.state.childCount < this.state.availableCount)
//     {
//         this.setState({childCount : (this.state.childCount+1), availableCount : (this.state.availableCount-1)})
//     }
// }

// childDecrement = () => {

// }


  render() {
      const { roomCount, adultCount, childCount} = this.state;

    return ( 
          <div className = "main">
                <label id = "one"><i class="fas fa-users"></i>choose number of people</label>
                <br/>
                  
                  <div className = "container">
                      
                      <div className = "rooms">
                          <label className = "leftSide"><i class="fas fa-bed"></i> Rooms</label>

                          <div className = "rightSide">
      
                              <button onClick = {this.roomDecrement} disabled={roomCount === 1}><i id = "iconOne" className ="fas fa-minus-circle"></i></button>
                              <span id = "spanRoomCount"> {this.state.roomCount} </span>
                              <button onClick = {this.roomIncrement} disabled={roomCount === 5}><i id = "iconTwo" className ="fas fa-plus-circle"></i></button>
                          </div>

                          <hr></hr>
                      </div>

                      <div className = "adults">
                      
                          <label className = "leftSide"><i className ="fas fa-user"></i> Adults</label>

                          <div className = "rightSide">

                              <button onClick = {this.adultDecrement} disabled={adultCount === 1}><i id = "iconOne" className ="fas fa-minus-circle"></i></button>
                              <span id = "spanAdultCount"> {this.state.adultCount} </span>
                              <button onClick = {this.adultIncrement} disabled={adultCount + childCount === 20}><i id = "iconTwo" className ="fas fa-plus-circle"></i></button>
                          </div>
                      </div>
                      <hr></hr>

                      <div className = "children">
                      
                          <label className = "leftSide"><i class="fas fa-child"></i> Children</label>

                          <div className = "rightSide">
                          
                              <button onClick = {this.childDecrement} disabled={childCount === 0}><i id = "iconOne" className ="fas fa-minus-circle"></i></button>
                              <span id = "spanChildCount"> {this.state.childCount} </span>
                              <button onClick = {this.childIncrement} disabled={adultCount + childCount === 20}><i id = "iconTwo" className ="fas fa-plus-circle"></i></button>
                          
                          </div>
                      </div>
                  </div>
          </div>

         );
  }
}

export default App;
